﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MyBuyProject.Application.Modules.Users.Commands.Delete;
using MyBuyProject.Application.Modules.Users.Commands.Insert;
using MyBuyProject.Application.Modules.Users.Commands.Update;
using MyBuyProject.Application.Modules.Users.Queries.GetUser;
using MyBuyProject.Application.Modules.Users.Queries.GetUsers;

namespace MyBuyProject.Web.Features.Users
{
    public class UsersController: Controller
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<IActionResult> Index()
        {
            var users = await _mediator.Send(new GetUsersQuery());

            return View(users);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] InsertUserCommand request, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return View();

            await _mediator.Send(request, cancellationToken);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var user = await _mediator.Send(new GetUserQuery() { Id = id });

            return View(user);
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var user = await _mediator.Send(new GetUserQuery { Id = id });

            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, [FromForm] UpdateUserCommand request)
        {
            if (!ModelState.IsValid)
                return View();

            await _mediator.Send(request);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Delete([FromForm] DeleteUserCommand request)
        {
            await _mediator.Send(request);

            return RedirectToAction(nameof(Index));
        }
    }
}
