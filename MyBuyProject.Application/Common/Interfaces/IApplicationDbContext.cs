﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyBuyProject.Domain.Entities.Orders;
using MyBuyProject.Domain.Entities.Products;
using MyBuyProject.Domain.Entities.Users;

namespace MyBuyProject.Application.Common.Interfaces
{
    public interface  IApplicationDbContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<Order> Orders { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
