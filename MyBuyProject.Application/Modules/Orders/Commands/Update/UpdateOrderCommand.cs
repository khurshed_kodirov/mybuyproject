﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace MyBuyProject.Application.Modules.Orders.Commands.Update
{
    public class UpdateOrderCommand : IRequest<bool>
    {
        public Guid Id;
        public Guid ProductId { get; set; }
        public Guid UserId { get; set; }
    }
}
