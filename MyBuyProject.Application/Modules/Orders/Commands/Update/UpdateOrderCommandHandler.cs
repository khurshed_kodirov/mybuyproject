﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MyBuyProject.Application.Common.Interfaces;
using MyBuyProject.Application.Modules.Orders.Notifications.Updated;

namespace MyBuyProject.Application.Modules.Orders.Commands.Update
{
    public class UpdateOrderCommandHandler : IRequestHandler<UpdateOrderCommand, bool>
    {
        private readonly IMediator _mediator;
        private readonly IApplicationDbContext _dbContext;

        public UpdateOrderCommandHandler(IMediator mediator, IApplicationDbContext dbContext)
        {
            _mediator = mediator;
            _dbContext = dbContext;
        }
        public async Task<bool> Handle(UpdateOrderCommand request, CancellationToken cancellationToken)
        {
            var order = await _dbContext.Orders.FirstOrDefaultAsync(o => o.Id == request.Id, cancellationToken);
            order.ProductId = request.ProductId;
            order.UserId = request.UserId;
            order.UpdatedAt = DateTime.Now;
            
            await _dbContext.SaveChangesAsync(cancellationToken);

            await _mediator.Publish(new UpdatedOrderNotification(order), cancellationToken);

            return true;
        }
    }
}
