﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using MyBuyProject.Domain.Entities.Orders;

namespace MyBuyProject.Application.Modules.Orders.Commands.Insert
{
    public class InsertOrderCommand : IRequest<bool>
    {
        public Guid ProductId { get; set; }
        public Guid UserId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
