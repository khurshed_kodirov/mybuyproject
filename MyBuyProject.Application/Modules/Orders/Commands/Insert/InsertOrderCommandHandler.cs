﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc.Formatters.Internal;
using MyBuyProject.Application.Common.Interfaces;
using MyBuyProject.Application.Modules.Orders.Notifications.Inserted;
using MyBuyProject.Domain.Entities.Orders;

namespace MyBuyProject.Application.Modules.Orders.Commands.Insert
{
    public class InsertOrderCommandHandler : IRequestHandler<InsertOrderCommand, bool>
    {
        private readonly IMediator _mediator;
        private readonly IApplicationDbContext _dbContext;

        public InsertOrderCommandHandler(IMediator mediator, IApplicationDbContext dbContext)
        {
            _mediator = mediator;
            _dbContext = dbContext;
        }
        public async Task<bool> Handle(InsertOrderCommand request, CancellationToken cancellationToken)
        {
            var order = new Order
            {
                Id = Guid.NewGuid(),
                ProductId = request.ProductId,
                UserId = request.UserId,
                CreatedAt = DateTime.Now
            };

            _dbContext.Orders.Add(order);
            await _dbContext.SaveChangesAsync(cancellationToken);
            
            await _mediator.Publish(new InsertedOrderNotification(order.Id), cancellationToken);

            return true;
        }
    }
}
