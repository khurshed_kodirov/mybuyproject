﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace MyBuyProject.Application.Modules.Orders.Commands.Delete
{
    public class DeleteOrderCommand : IRequest<bool>
    {
        public Guid Id;
    }
}
