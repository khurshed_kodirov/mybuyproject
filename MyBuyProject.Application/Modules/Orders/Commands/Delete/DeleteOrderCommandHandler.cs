﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MyBuyProject.Application.Common.Interfaces;
using MyBuyProject.Application.Modules.Orders.Notifications.Deleted;

namespace MyBuyProject.Application.Modules.Orders.Commands.Delete
{
    public class DeleteOrderCommandHandler : IRequestHandler<DeleteOrderCommand, bool>
    {
        private readonly IMediator _mediator;
        private readonly IApplicationDbContext _dbContext;

        public DeleteOrderCommandHandler(IMediator mediator, IApplicationDbContext dbContext)
        {
            _mediator = mediator;
            _dbContext = dbContext;
        }
        public async Task<bool> Handle(DeleteOrderCommand request, CancellationToken cancellationToken)
        {
            var order = await _dbContext.Orders.FirstOrDefaultAsync(o => o.Id == request.Id, cancellationToken);
            _dbContext.Orders.RemoveRange(order);
            await _dbContext.SaveChangesAsync(cancellationToken);

            await _mediator.Publish(new DeletedOrderNotification(order.Id), cancellationToken);

            return true;
        }
    }
}
