﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using MyBuyProject.Application.Modules.Orders.Commands.Insert;

namespace MyBuyProject.Application.Modules.Orders.Notifications.Inserted
{
    public class InsertedOrderNotificationHandler : INotificationHandler<InsertedOrderNotification>
    {
        private readonly ILogger<InsertOrderCommand> _logger;

        public InsertedOrderNotificationHandler(ILogger<InsertOrderCommand> logger)
        {
            _logger = logger;
        }
        public Task Handle(InsertedOrderNotification notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Added new order", notification.OrderId);
            return Task.CompletedTask;
        }
    }
}
