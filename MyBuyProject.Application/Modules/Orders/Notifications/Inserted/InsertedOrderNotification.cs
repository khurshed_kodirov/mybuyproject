﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace MyBuyProject.Application.Modules.Orders.Notifications.Inserted
{
    public class InsertedOrderNotification : INotification
    {
        public Guid OrderId { get; }

        public InsertedOrderNotification(Guid orderId)
        {
            OrderId = orderId;
        }
    }
}
