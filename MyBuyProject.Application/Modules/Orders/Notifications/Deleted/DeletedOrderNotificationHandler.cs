﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;

namespace MyBuyProject.Application.Modules.Orders.Notifications.Deleted
{
    public class DeletedOrderNotificationHandler : INotificationHandler<DeletedOrderNotification>
    {
        private readonly ILogger<DeletedOrderNotification> _logger;

        public DeletedOrderNotificationHandler(ILogger<DeletedOrderNotification> logger)
        {
            _logger = logger;
        }
        public Task Handle(DeletedOrderNotification notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Deleted order", notification.OrderId);
            return Task.CompletedTask;
        }
    }
}
