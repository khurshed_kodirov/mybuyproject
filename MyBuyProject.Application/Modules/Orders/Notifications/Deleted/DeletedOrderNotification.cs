﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace MyBuyProject.Application.Modules.Orders.Notifications.Deleted
{
    public class DeletedOrderNotification : INotification
    {
        public readonly Guid OrderId;

        public DeletedOrderNotification(Guid orderId)
        {
            OrderId = orderId;
        }
    }
}
