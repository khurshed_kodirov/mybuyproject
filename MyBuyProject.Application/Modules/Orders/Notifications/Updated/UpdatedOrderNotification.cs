﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using MyBuyProject.Domain.Entities.Orders;

namespace MyBuyProject.Application.Modules.Orders.Notifications.Updated
{
    public class UpdatedOrderNotification : INotification
    {
        public Order Order { get; }
        public UpdatedOrderNotification(Order order)
        {
            Order = order;
        }
    }
}
