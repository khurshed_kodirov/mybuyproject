﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;

namespace MyBuyProject.Application.Modules.Orders.Notifications.Updated
{
    public class UpdatedOrderNotificationHandler : INotificationHandler<UpdatedOrderNotification>
    {
        private readonly ILogger<UpdatedOrderNotification> _logger;

        public UpdatedOrderNotificationHandler(ILogger<UpdatedOrderNotification> logger)
        {
            _logger = logger;
        }
        public Task Handle(UpdatedOrderNotification notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Updated order", notification.Order);
            return Task.CompletedTask;
        }
    }
}
