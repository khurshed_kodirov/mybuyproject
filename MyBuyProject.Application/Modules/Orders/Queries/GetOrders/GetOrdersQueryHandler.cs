﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MyBuyProject.Application.Common.Interfaces;
using MyBuyProject.Domain.Entities.Orders;

namespace MyBuyProject.Application.Modules.Orders.Queries.GetOrders
{
    public class GetOrdersQueryHandler : IRequestHandler<GetOrdersQuery, List<GetOrdersViewModel>>
    {
        private readonly IMapper _mapper;
        private readonly IApplicationDbContext _dbContext;

        public GetOrdersQueryHandler(IMapper mapper, IApplicationDbContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }
        public async Task<List<GetOrdersViewModel>> Handle(GetOrdersQuery request, CancellationToken cancellationToken)
        {
            var orders = await _dbContext
                .Orders
                .Include(o=>o.Product)
                .Include(o=>o.User)
                .ToListAsync(cancellationToken);

            return _mapper.Map<List<GetOrdersViewModel>>(orders);
        }
    }
}
