﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MyBuyProject.Domain.Entities.Orders;

namespace MyBuyProject.Application.Modules.Orders.Queries.GetOrders
{
    public class GetOrdersMapper : Profile
    {
        public GetOrdersMapper()
        {
            CreateMap<Order, GetOrdersViewModel>()
                .ForMember(vm => vm.Product, opt => opt.MapFrom(src => src.Product.Name))
                .ForMember(vm => vm.User, opt => opt.MapFrom(src => src.User.Name));
        }
    }
}
