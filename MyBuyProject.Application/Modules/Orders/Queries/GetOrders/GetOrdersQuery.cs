﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace MyBuyProject.Application.Modules.Orders.Queries.GetOrders
{
    public class GetOrdersQuery : IRequest<List<GetOrdersViewModel>>
    {
    }
}
