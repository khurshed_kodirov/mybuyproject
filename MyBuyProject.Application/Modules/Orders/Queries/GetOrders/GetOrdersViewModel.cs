﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBuyProject.Application.Modules.Orders.Queries.GetOrders
{
    public class GetOrdersViewModel
    {
        public Guid Id { get; set; }
        public string Product { get; set; }
        public string User { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
