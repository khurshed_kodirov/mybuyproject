﻿using System;

namespace MyBuyProject.Application.Modules.Orders.Queries.GetOrder
{
    public class GetOrderViewModel
    {
        public Guid Id { get; set; }
        public string Product { get; set; }
        public string User { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
