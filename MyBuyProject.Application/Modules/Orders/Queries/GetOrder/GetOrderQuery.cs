﻿using System;
using MediatR;

namespace MyBuyProject.Application.Modules.Orders.Queries.GetOrder
{
    public class GetOrderQuery : IRequest<GetOrderViewModel>
    {
        public Guid Id;
    }
}
