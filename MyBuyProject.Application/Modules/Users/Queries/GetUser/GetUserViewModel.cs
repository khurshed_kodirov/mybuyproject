﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBuyProject.Application.Modules.Users.Queries.GetUser
{
    public class GetUserViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
