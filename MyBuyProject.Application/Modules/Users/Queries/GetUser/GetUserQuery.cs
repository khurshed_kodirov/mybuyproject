﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace MyBuyProject.Application.Modules.Users.Queries.GetUser
{
    public class GetUserQuery : IRequest<GetUserViewModel>
    {
        public Guid Id;
    }
}
