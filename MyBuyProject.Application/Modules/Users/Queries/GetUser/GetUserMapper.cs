﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MyBuyProject.Domain.Entities.Users;

namespace MyBuyProject.Application.Modules.Users.Queries.GetUser
{
    public class GetUserMapper : Profile
    {
        public GetUserMapper()
        {
            CreateMap<User, GetUserViewModel>();
        }
    }
}
