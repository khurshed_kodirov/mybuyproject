﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MyBuyProject.Domain.Entities.Users;

namespace MyBuyProject.Application.Modules.Users.Queries.GetUsers
{
    public class GetUsersMapper : Profile
    {
        public GetUsersMapper()
        {
            CreateMap<User, GetUsersViewModel>();
        }
    }
}
