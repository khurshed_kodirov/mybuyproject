﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace MyBuyProject.Application.Modules.Users.Queries.GetUsers
{
    public class GetUsersQuery : IRequest<List<GetUsersViewModel>>
    {
    }
}
