﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBuyProject.Application.Modules.Products.Queries.GetProducts
{
    public class GetProductsViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
