﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MyBuyProject.Application.Common.Interfaces;

namespace MyBuyProject.Application.Modules.Products.Queries.GetProducts
{
    public class GetProductsQueryHandler:IRequestHandler<GetProductsQuery,List<GetProductsViewModel>>
    {
        private readonly IMapper _mapper;
        private readonly IApplicationDbContext _dbContext;

        public GetProductsQueryHandler(IMapper mapper, IApplicationDbContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }
        public async Task<List<GetProductsViewModel>> Handle(GetProductsQuery request, CancellationToken cancellationToken)
        {
            var products = await _dbContext.Products.ToListAsync(cancellationToken);
            return _mapper.Map<List<GetProductsViewModel>>(products);
        }
    }
}
