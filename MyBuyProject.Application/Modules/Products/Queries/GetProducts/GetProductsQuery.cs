﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace MyBuyProject.Application.Modules.Products.Queries.GetProducts
{
    public class GetProductsQuery : IRequest<List<GetProductsViewModel>>
    {
    }
}
