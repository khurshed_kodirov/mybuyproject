﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MyBuyProject.Domain.Entities.Products;

namespace MyBuyProject.Application.Modules.Products.Queries.GetProducts
{
    public class GetProductsMapper : Profile
    {
        public GetProductsMapper()
        {
            CreateMap<Product, GetProductsViewModel>();
        }
    }
}
