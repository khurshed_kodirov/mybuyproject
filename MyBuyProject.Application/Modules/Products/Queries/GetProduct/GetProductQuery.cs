﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace MyBuyProject.Application.Modules.Products.Queries.GetProduct
{
    public class GetProductQuery : IRequest<GetProductViewModel>
    {
        public Guid Id;
    }
}
