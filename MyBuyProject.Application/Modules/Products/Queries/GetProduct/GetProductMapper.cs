﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using MyBuyProject.Domain.Entities.Products;

namespace MyBuyProject.Application.Modules.Products.Queries.GetProduct
{
    public class GetProductMapper : Profile
    {
        public GetProductMapper()
        {
            CreateMap<Product, GetProductViewModel>();
        }
    }
}
