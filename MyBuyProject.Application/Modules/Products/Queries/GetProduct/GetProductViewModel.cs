﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBuyProject.Application.Modules.Products.Queries.GetProduct
{
    public class GetProductViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
