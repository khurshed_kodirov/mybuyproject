﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBuyProject.Domain.Entities
{
    public abstract class EntityBase
    {
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
