﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBuyProject.Domain.Entities.Users
{
    public class User : EntityBase
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
