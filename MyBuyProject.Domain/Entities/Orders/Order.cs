﻿using System;
using System.Collections.Generic;
using System.Text;
using MyBuyProject.Domain.Entities.Products;
using MyBuyProject.Domain.Entities.Users;

namespace MyBuyProject.Domain.Entities.Orders
{
    public class Order : EntityBase
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid UserId { get; set; }

        public Product Product { get; set; }
        public User User { get; set; }
    }
}
