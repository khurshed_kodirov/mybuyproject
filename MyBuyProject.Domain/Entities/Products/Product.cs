﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyBuyProject.Domain.Entities.Products
{
    public class Product : EntityBase
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
