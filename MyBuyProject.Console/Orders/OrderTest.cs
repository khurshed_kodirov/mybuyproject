﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using MyBuyProject.Application.Modules.Orders.Commands.Delete;
using MyBuyProject.Application.Modules.Orders.Commands.Insert;
using MyBuyProject.Application.Modules.Orders.Commands.Update;
using MyBuyProject.Application.Modules.Orders.Queries.GetOrder;
using MyBuyProject.Application.Modules.Orders.Queries.GetOrders;

namespace MyBuyProject.Console.Orders
{
    public class OrderTest
    {
        private readonly IMediator _mediator;

        public OrderTest()
        {
            var service = new ServiceCollection();
            service.AddMediatR(typeof(InsertOrderCommand));
            service.AddMediatR(typeof(UpdateOrderCommand));
            service.AddMediatR(typeof(DeleteOrderCommand));

            service.AddMediatR(typeof(GetOrderQuery));
            service.AddMediatR(typeof(GetOrdersQuery));

            _mediator = service.BuildServiceProvider().GetService<IMediator>();
        }

        public async Task<GetOrderViewModel> GetById(Guid id)
        {
            var order = await _mediator.Send(new GetOrderQuery { Id = id });
            return order;
        }

        public async Task<List<GetOrdersViewModel>> GetOrders()
        {
            var orders = await _mediator.Send(new GetOrdersQuery());
            return orders;
        }

        public async Task<bool> Create(InsertOrderCommand request)
        {
            await _mediator.Send(request);
            return true;
        }

        public async Task<bool> Update(UpdateOrderCommand request)
        {
            await _mediator.Send(request);
            return true;
        }

        public async Task<bool> Delete(DeleteOrderCommand request)
        {
            await _mediator.Send(request);
            return true;
        }
    }
}
