﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using MyBuyProject.Application.Modules.Orders.Commands.Delete;
using MyBuyProject.Application.Modules.Orders.Commands.Insert;
using MyBuyProject.Application.Modules.Orders.Commands.Update;
using MyBuyProject.Application.Modules.Orders.Queries.GetOrder;
using MyBuyProject.Application.Modules.Orders.Queries.GetOrders;
using MyBuyProject.Application.Modules.Products.Commands.Detele;
using MyBuyProject.Application.Modules.Products.Commands.Insert;
using MyBuyProject.Application.Modules.Products.Commands.Update;
using MyBuyProject.Application.Modules.Products.Queries.GetProduct;
using MyBuyProject.Application.Modules.Products.Queries.GetProducts;

namespace MyBuyProject.Console.Products
{
    public class ProductTest
    {
        private readonly IMediator _mediator;
        public ProductTest()
        {
            var service = new ServiceCollection();
            service.AddMediatR(typeof(InsertProductCommand));
            service.AddMediatR(typeof(UpdateProductCommand));
            service.AddMediatR(typeof(DeleteProductCommand));

            service.AddMediatR(typeof(GetProductQuery));
            service.AddMediatR(typeof(GetProductsQuery));
            
            _mediator = service.BuildServiceProvider().GetService<IMediator>();
        }

        public async Task<GetProductViewModel> GetById(Guid id)
        {
            var product = await _mediator.Send(new GetProductQuery { Id = id });
            return product;
        }

        public async Task<List<GetProductsViewModel>> GetOrders()
        {
            var products = await _mediator.Send(new GetProductsQuery());
            return products;
        }

        public async Task<bool> Create(InsertProductCommand request)
        {
            await _mediator.Send(request);
            return true;
        }

        public async Task<bool> Update(UpdateProductCommand request)
        {
            await _mediator.Send(request);
            return true;
        }

        public async Task<bool> Delete(DeleteProductCommand request)
        {
            await _mediator.Send(request);
            return true;
        }
    }
}
