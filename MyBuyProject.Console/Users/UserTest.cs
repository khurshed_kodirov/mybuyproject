﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using MyBuyProject.Application.Modules.Products.Commands.Detele;
using MyBuyProject.Application.Modules.Products.Commands.Insert;
using MyBuyProject.Application.Modules.Products.Commands.Update;
using MyBuyProject.Application.Modules.Products.Queries.GetProduct;
using MyBuyProject.Application.Modules.Products.Queries.GetProducts;
using MyBuyProject.Application.Modules.Users.Commands.Delete;
using MyBuyProject.Application.Modules.Users.Commands.Insert;
using MyBuyProject.Application.Modules.Users.Commands.Update;
using MyBuyProject.Application.Modules.Users.Queries.GetUser;
using MyBuyProject.Application.Modules.Users.Queries.GetUsers;

namespace MyBuyProject.Console.Users
{
    public class UserTest
    {
        private readonly IMediator _mediator;
        public UserTest()
        {
            var service = new ServiceCollection();
            service.AddMediatR(typeof(InsertUserCommand));
            service.AddMediatR(typeof(UpdateUserCommand));
            service.AddMediatR(typeof(DeleteUserCommand));

            service.AddMediatR(typeof(GetUserQuery));
            service.AddMediatR(typeof(GetUsersQuery));

            _mediator = service.BuildServiceProvider().GetService<IMediator>();
        }

        public async Task<GetUserViewModel> GetById(Guid id)
        {
            var user = await _mediator.Send(new GetUserQuery { Id = id });
            return user;
        }

        public async Task<List<GetUsersViewModel>> GetOrders()
        {
            var users = await _mediator.Send(new GetUsersQuery());
            return users;
        }

        public async Task<bool> Create(InsertUserCommand request)
        {
            await _mediator.Send(request);
            return true;
        }

        public async Task<bool> Update(UpdateUserCommand request)
        {
            await _mediator.Send(request);
            return true;
        }

        public async Task<bool> Delete(DeleteUserCommand request)
        {
            await _mediator.Send(request);
            return true;
        }
    }
}
