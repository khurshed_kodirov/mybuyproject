﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MyBuyProject.Application.Common.Interfaces;
using MyBuyProject.Domain.Entities.Orders;
using MyBuyProject.Domain.Entities.Products;
using MyBuyProject.Domain.Entities.Users;

namespace MyBuyProject.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}
